
import java.util.Arrays;
public class MT {
private double[] pos = new double[3];
private double[] vel = new double[3];
public float[] antennadir = new float[2];
String Estado;
public float[] DireccionOptima = new float[2];
private double Poptima;
private int PrioritySector=1;
private int FaseAlineacion=1;
private boolean PrimeraAlineacion;
private int assignedBSAntenna;
private float Gmax;
private float Power;
private int TiempoTx;
private int TBackoff;
public int SectoresEscaneados;
public double HPBW;
public MT(double[] posinicial, float Ganancia, float potencia, double[] velocid){
	this.setPos(posinicial);
	this.Gmax=Ganancia;
	this.Power=potencia;
	this.setVel(velocid);
	this.assignedBSAntenna=-1;
	this.PrioritySector=4;
	this.Estado="Standby";
	this.Poptima=-100;
	this.HPBW=Math.sqrt(31000*Math.pow(10, -this.Gmax/10));
}
public float getPow() {
	return Power;
}
public void setPow(float Potencia) {
	this.Power=Potencia;
}
public double[] getPos() {
	return pos;
}
public void setPos(double[] pos) {
	this.pos = Arrays.copyOf(pos, 3);
}
public int getTiempoTx() {
	return TiempoTx;
}
public void setTiempoTx(int TTX) {
	this.TiempoTx = TTX;
}
public int getTBackoff() {
	return TBackoff;
}
public void setTBackoff(int Backoff) {
	this.TBackoff = Backoff;
}
public int getAssignedAntenna() {
	return assignedBSAntenna;
}
public void setAssignedAntenna(int Antena) {
	this.assignedBSAntenna = Antena;
}
public double getPoptima() {
	return Poptima;
}
public void setPoptima(double Poptim) {
	this.Poptima = Poptim;
}
public double[] getVel() {
	return vel;
}

public void setVel(double[] ve) {
	this.vel = Arrays.copyOf(ve,3);
}
public int getPrioritySector() {
	return PrioritySector;
}
public void setPrioritySector(int Sector) {
	this.PrioritySector = Sector;
}
public int getFaseAlineacion() {
	return FaseAlineacion;
}
public void setFaseAlineacion(int FaseAline) {
	this.FaseAlineacion = FaseAline;
}
public int getScannedSectors() {
	return SectoresEscaneados;
}
public void setScannedSectors(int Escaneados) {
	this.SectoresEscaneados = Escaneados;
}
public float[] getAntennadir() {
	return antennadir;
}
public void setAntennadir(float[] antenndir) {
	this.antennadir = Arrays.copyOf(antenndir,2);
}
public boolean getPrimeraAlineacion() {
	return PrimeraAlineacion;
}
public void setPrimeraAlineacion(boolean Primera) {
	this.PrimeraAlineacion = Primera;
}
public float[] getDireccionOptima() {
	return DireccionOptima;
}
public void setDireccionOptima(float[] DireccionOptim) {
	this.DireccionOptima = Arrays.copyOf(DireccionOptim, 2);
}
public void setState(String Stat){
	this.Estado = Stat;
}

public String getState(){
	return Estado;
}
public double GetGanreal(double[] posNodoB){
	double[] direcreal=new double [2];
	double[] posrel=new double[3];
	posrel[0]=posNodoB[0]-this.pos[0];
	posrel[1]=posNodoB[1]-this.pos[1];
	posrel[2]=posNodoB[2]-this.pos[2];
	direcreal[0]=Math.atan(posrel[1]/posrel[0]);
	direcreal[1]=Math.atan(posrel[2]/Math.sqrt(posrel[0]*posrel[0]+posrel[1]*posrel[1]));
	if(posrel[0]<0){
		if(posrel[1]<0){
			direcreal[0]=direcreal[0]-Math.PI;
		}
		else{
			direcreal[0]=Math.PI-direcreal[0];
		}
	}
	double[] direcrelativa=new double[2];
	direcrelativa[0]=direcreal[0]-Math.toRadians(this.antennadir[0]);
	direcrelativa[1]=direcreal[1]-Math.toRadians(this.antennadir[1]);
	double Ganreal = 0;
	double N=Math.toDegrees(Math.acos(Math.cos(direcrelativa[0])*Math.cos(direcrelativa[1])));
	double Na=0;
	double HPBW2=0;
	if (N<90){
		Na= this.HPBW;
	}
	else if (N<180){
		if(Math.abs(direcrelativa[0])<Math.toRadians(this.HPBW)){
			HPBW2=this.HPBW;
		}
		else if(Math.abs(direcrelativa[0])<Math.PI){
	HPBW2=1/Math.sqrt((Math.cos((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)*(Math.cos((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)+(Math.sin((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)*(Math.sin((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW));
		}
		Na= 1/(Math.sqrt((Math.cos(direcrelativa[1])/HPBW2)*(Math.cos(direcrelativa[1])/HPBW2)+(Math.sin(direcrelativa[1])/this.HPBW)*(Math.sin(direcrelativa[1])/this.HPBW)));
	}
	double x=N/Na;
	if (x<=1){
		Ganreal=Gmax-12*x*x;
	}
	else{
		Ganreal=Gmax-12-15*Math.log(Math.abs(x));
	}
	return Ganreal;
}
public double Rx(double Ganreal, float GanBS, double[] posNodoB, float powBS){
	double[] posrel=new double[3];
	posrel[0]=posNodoB[0]-this.pos[0];
	posrel[1]=posNodoB[1]-this.pos[1];
	posrel[2]=posNodoB[2]-this.pos[2];
	double distancia=0.001*Math.sqrt(posrel[0]*posrel[0]+posrel[1]*posrel[1]+posrel[2]*posrel[2]);
	double FSL=92.44+20*Math.log10(60)+22*Math.log10(distancia);
	double Powrec=(powBS-FSL+Ganreal+GanBS);
	return Powrec;
}
public void ActualizacionOptima(double PotenciaActual){
	if (PotenciaActual>this.Poptima){
		this.Poptima=PotenciaActual;
		this.DireccionOptima=Arrays.copyOf(this.antennadir, 2);
		}
}
}
