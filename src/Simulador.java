import java.util.Arrays;
public class Simulador {
	public Simulador() {}
	
	int SesionesExitosas;
	int SesionesFallidas;
	int TransmisionesExitosas;
	int TransmisionesFallidas;
	int AlineacionesExitosas;
	int AlineacionesFallidas;
	float[] Direccion= new float[3];
	float[] DataFinal=new float[6];
	float[] DataInicio= new float[16];
	float[] Simulacion(double[] DataInicio){
		for (int sim=0; sim<DataInicio[7];sim++ ){
	int NMoviles=(int)DataInicio[0];
	MT[] ListaMoviles=new MT[NMoviles];
	int NArray=(int)DataInicio[13];
	BaseStation EstacionBase=new BaseStation(NArray, (float)DataInicio[6], (float)DataInicio[2]);
	double[] postransitoria= new double[3];
	double GMT= 0;
	double GBS= 0;
	double Pr=0;
	double MejorPr=0;
	int AntenaAsignada=0;
	int tmax=(int)DataInicio[14];
	float SR =(float)DataInicio[3];
	double PR=0;
	double Interference=0;
	double SINR= 0;
	boolean PrimerAlineacion;
	float PowBS;
	float PowMT;
	double[] pos;
	double[] vel;
	double BestSINR;
	int Back;
	int Packets;
	int NextSector;
	int Numeral;
	double InterferencePow;
	float[] Boundary=new float[2];
	for (int n=0; n<NMoviles; n++){
		double[] posicion= new double[3];
		double[] velocidad= new double[3];

		for (int m=0; m<3; m++){
			posicion[m]=(Math.random()*DataInicio[4+m]);
			velocidad[m]=(Math.random()*DataInicio[9+m]);
		}
		ListaMoviles[n]=new MT(posicion, (float)DataInicio[2], (float)DataInicio[1], velocidad);	
		System.out.println("La posicion del MT N� "+n+" es " +posicion[0]+" "+posicion[1]+" "+posicion[2]);
		}
for (int t=0; t<tmax; t++){
	Numeral=0;
	for (MT TerminalActual : ListaMoviles){
		Numeral=Numeral+1;
	if (TerminalActual.Estado.equals("Transmision")){
		if(TerminalActual.getTiempoTx()>0){
		for (MT RecepcionBS:ListaMoviles){
			if(RecepcionBS==TerminalActual){
				postransitoria=	EstacionBase.getPos();
				GMT= RecepcionBS.GetGanreal(postransitoria);
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= RecepcionBS.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);
				PowMT = RecepcionBS.getPow();
				PR= EstacionBase.Rx(GBS, GMT, postransitoria, PowMT);
			}
			else{
			if(RecepcionBS.getState()=="Transmision"){
				postransitoria=	EstacionBase.getPos();
				GMT= RecepcionBS.GetGanreal(postransitoria);
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= TerminalActual.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);
				PowMT = RecepcionBS.getPow();
				Interference= Interference + Math.pow(10d,(EstacionBase.Rx(GBS, GMT, postransitoria, PowMT))/10);
			}
			else if(RecepcionBS.getState()=="Alineacion"){
				if(RecepcionBS.getFaseAlineacion()==2){
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= TerminalActual.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);
				PowMT = RecepcionBS.getPow();
				Interference= Interference + Math.pow(10d,(EstacionBase.Rx(GBS, 0, postransitoria, PowMT))/10);
				}
			}
			}
		}
			PR=Math.pow(10d, PR/10);
			double noise=2.72*Math.pow(10d,-10d);
			SINR=10*Math.log10(PR/(noise+Interference));
			Interference=0;
			if (SINR<22.7){
				TerminalActual.setState("Backoff");
				this.TransmisionesFallidas++;
				this.SesionesFallidas++;
				Back=(int)(Math.random()*DataInicio[8]);
				TerminalActual.setTBackoff(Back);
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=0;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;
				TerminalActual.setAssignedAntenna(-1);
			}
			else{
				this.TransmisionesExitosas++;
			}
		TerminalActual.setTiempoTx(TerminalActual.getTiempoTx()-1);
	}
		if (TerminalActual.getTiempoTx()==0){
			TerminalActual.setState("Backoff");
			this.SesionesExitosas++;
			Back=(int)(2*Math.random()*DataInicio[8]);
			TerminalActual.setTBackoff(Back);
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=0;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;
			TerminalActual.setAssignedAntenna(-1);
		}
		}
if (TerminalActual.Estado.equals("Alineacion")){
	int Fase=TerminalActual.getFaseAlineacion();
	if(Fase==1){
		int Sector=TerminalActual.getPrioritySector();
		PrimerAlineacion = TerminalActual.getPrimeraAlineacion();
		if(Sector==1){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=0;
				TerminalActual.antennadir[1]=0;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<90){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>90){
					TerminalActual.antennadir[0]=0;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>90){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(2);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
							TerminalActual.setPrioritySector(1);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima, 2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		
		if(Sector==2){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=90;
				TerminalActual.antennadir[1]=0;
			}
			TerminalActual.setPrimeraAlineacion(false);;
	
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);

			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			MejorPr=TerminalActual.getPoptima();
			TerminalActual.ActualizacionOptima(Pr);
			
			if (TerminalActual.antennadir[0]<180){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>90){
					TerminalActual.antennadir[0]=90;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>90){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
								this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(3);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
	TerminalActual.setPrioritySector(2);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==3){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=-90;
				TerminalActual.antennadir[1]=0;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<0){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>0){
					TerminalActual.antennadir[0]=-90;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>90){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(4);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
	TerminalActual.setPrioritySector(3);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==4){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=-180;
				TerminalActual.antennadir[1]=0;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<-90){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>-90){
					TerminalActual.antennadir[0]=-180;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>90){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(5);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
							}
						else{
							TerminalActual.setPrioritySector(4);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==5){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=0;
				TerminalActual.antennadir[1]=-90;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			MejorPr=TerminalActual.getPoptima();
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<90){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>90){
					TerminalActual.antennadir[0]=0;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>0){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(6);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
	TerminalActual.setPrioritySector(5);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
							EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==6){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=90;
				TerminalActual.antennadir[1]=-90;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<180){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>180){
					TerminalActual.antennadir[0]=90;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>0){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(7);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
	TerminalActual.setPrioritySector(6);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==7){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=-90;
				TerminalActual.antennadir[0]=-90;
			}
			TerminalActual.setPrimeraAlineacion(false);
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<0){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>0){
					TerminalActual.antennadir[0]=-90;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>0){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
							}
							else{
	TerminalActual.setPrioritySector(8);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
							}
						}
						else{
	TerminalActual.setPrioritySector(7);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	TerminalActual.setFaseAlineacion(2);
						}
					}
				}
			}
		}
		if(Sector==8){		
			if (PrimerAlineacion==true){
				TerminalActual.antennadir[0]=-180;
				TerminalActual.antennadir[1]=-90;
			}
			TerminalActual.setPrimeraAlineacion(false);;
			postransitoria=	EstacionBase.getPos();
			GMT = TerminalActual.GetGanreal(postransitoria);
			PowBS = EstacionBase.getPow();
			Pr=TerminalActual.Rx(GMT, 0f, postransitoria, PowBS);
			TerminalActual.ActualizacionOptima(Pr);
			if (TerminalActual.antennadir[0]<-90){
				TerminalActual.antennadir[0]=(float) (TerminalActual.antennadir[0]+2*TerminalActual.HPBW);
				if (TerminalActual.antennadir[0]>-90){
					TerminalActual.antennadir[0]=-180;
					TerminalActual.antennadir[1]=(float) (TerminalActual.antennadir[1]+2*TerminalActual.HPBW);
					if (TerminalActual.antennadir[1]>0){
						if(TerminalActual.getPoptima()<SR){
	if(TerminalActual.SectoresEscaneados==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
	TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	TerminalActual.SectoresEscaneados=0;
								}
								else{
	TerminalActual.setPrioritySector(1);
	TerminalActual.setPrimeraAlineacion(true);
	TerminalActual.SectoresEscaneados=TerminalActual.SectoresEscaneados+1;
								}
						}
						else{
	TerminalActual.setPrioritySector(8);
	TerminalActual.antennadir=Arrays.copyOf(TerminalActual.DireccionOptima,2);
	TerminalActual.SectoresEscaneados=0;
	TerminalActual.setFaseAlineacion(2);
	EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
						}
					}
				}
			}
		}
}
	if (Fase==2){
		if (EstacionBase.getAlineationStart(TerminalActual.getAssignedAntenna())==false){
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==1){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=0;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;
				Boundary[0]=90;
				Boundary[1]=90;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==2){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=90;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;	
				Boundary[0]=180;
				Boundary[1]=90;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==3){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=-90;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;
				Boundary[0]=0;
				Boundary[1]=90;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==4){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=-180;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=0;
				Boundary[0]=-90;
				Boundary[1]=90;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==5){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=0;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=-90;
				Boundary[0]=90;
				Boundary[1]=0;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==6){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=90;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=-90;
				Boundary[0]=180;
				Boundary[1]=0;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==7){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=-90;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=-90;
				Boundary[0]=0;
				Boundary[1]=0;
			}
	if(EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==8){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=-180;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=-90;
				Boundary[0]=-90;
				Boundary[1]=0;
			}
		}
		EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), true);
		for (MT RecepcionBS:ListaMoviles){
			if(RecepcionBS==TerminalActual){
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= RecepcionBS.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);
				PowMT = RecepcionBS.getPow();
				PR= EstacionBase.Rx(GBS, 0, postransitoria, PowMT);
			}
			else{
			if(RecepcionBS.getState()=="Transmision"){
				postransitoria=	EstacionBase.getPos();
				GMT= RecepcionBS.GetGanreal(postransitoria);
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= TerminalActual.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);
				PowMT = RecepcionBS.getPow();
				InterferencePow=EstacionBase.Rx(GBS, GMT, postransitoria, PowMT);
				Interference= Interference + Math.pow(10d,InterferencePow/10);
			}
			if(RecepcionBS.getState()=="Alineacion"){
				if(RecepcionBS.getFaseAlineacion()==2){
				postransitoria=	RecepcionBS.getPos();
				AntenaAsignada= TerminalActual.getAssignedAntenna();
				GBS = EstacionBase.GetGanreal(postransitoria,AntenaAsignada);

				PowMT = RecepcionBS.getPow();
				InterferencePow=EstacionBase.Rx(GBS, 0, postransitoria, PowMT);
				Interference= Interference + Math.pow(10d,InterferencePow/10);
				}
			}
			}
		}
			PR=Math.pow(10d, PR/10);
			double noise=2.72*Math.pow(10d,-10d);
			SINR=10*Math.log10(PR/(noise+Interference));
			Interference=0;
	BestSINR=EstacionBase.getSINROptimo(TerminalActual.getAssignedAntenna());
		if (SINR>BestSINR){
	EstacionBase.setSINROptimo(TerminalActual.getAssignedAntenna(), SINR);
	EstacionBase.AllOptimumDir[TerminalActual.getAssignedAntenna()]=Arrays.copyOf(EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()],2);
			}
		if (EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]<Boundary[0]){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=(float)(EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]+2*EstacionBase.HPBW);
if(EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]>Boundary[0]){
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][0]=Boundary[0]-90;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=(float)(EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]+2*EstacionBase.HPBW);
	if(EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]>Boundary[1]){
	if(EstacionBase.getSINROptimo(TerminalActual.getAssignedAntenna())<15){
	if(EstacionBase.SectoresEscaneados[TerminalActual.getAssignedAntenna()]==8){
	this.AlineacionesFallidas=this.AlineacionesFallidas+1;
	Back=(int)(2*Math.random()*DataInicio[8]);
							TerminalActual.setTBackoff(Back);
	TerminalActual.setState("Backoff");
	EstacionBase.SectoresEscaneados[TerminalActual.getAssignedAntenna()]=0;
						}
						else{
							if (EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())==8){
								NextSector=1;
							}
							else{
	NextSector=EstacionBase.getPrioritySector(TerminalActual.getAssignedAntenna())+1;
							}
		EstacionBase.setPrioritySector(TerminalActual.getAssignedAntenna(), NextSector);
		EstacionBase.setAlineationStart(TerminalActual.getAssignedAntenna(), false);
	EstacionBase.SectoresEscaneados[TerminalActual.getAssignedAntenna()]=EstacionBase.SectoresEscaneados[TerminalActual.getAssignedAntenna()]+1;
						}
					}
					else{
					TerminalActual.setState("Transmision");
					this.AlineacionesExitosas++;
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()]=Arrays.copyOf(EstacionBase.AllOptimumDir[TerminalActual.getAssignedAntenna()],2);
					Packets=(int)(Math.random()*DataInicio[15]);
					TerminalActual.setTiempoTx(Packets);
		EstacionBase.setSINROptimo(TerminalActual.getAssignedAntenna(),-100);
					}
				}
			}
		}
	}
}
if (TerminalActual.Estado.equals("Standby")){
	double prob=Math.random();
	if(prob<DataInicio[12]){
		TerminalActual.setState("Alineacion");
		TerminalActual.setPrimeraAlineacion(true);
		TerminalActual.setFaseAlineacion(1);
		TerminalActual.setAssignedAntenna(EstacionBase.getFreeArray());
		if (TerminalActual.getAssignedAntenna()==-1){
			TerminalActual.setState("Standby");
		}
		else{
	EstacionBase.AllAntennaDir[TerminalActual.getAssignedAntenna()][1]=1;
		}
	}
}
if (TerminalActual.Estado.equals("Backoff")){
	if(TerminalActual.getTBackoff()==0){
		TerminalActual.setState("Standby");
	}
	TerminalActual.setTBackoff(TerminalActual.getTBackoff()-1);
}
for(int o=0;o<3;o++){
	pos=Arrays.copyOf(TerminalActual.getPos(), 3);
	vel=Arrays.copyOf(TerminalActual.getVel(), 3);
	pos[o]=pos[o]+Math.pow(10d,-5d)*vel[o];
	TerminalActual.setPos(pos);
}
}
}
}
	DataFinal[0]=SesionesExitosas;
	DataFinal[1]=SesionesFallidas;
	DataFinal[2]=TransmisionesExitosas;
	DataFinal[3]=TransmisionesFallidas;
	DataFinal[4]=AlineacionesExitosas;
	DataFinal[5]=AlineacionesFallidas;
	return DataFinal;
}
}
