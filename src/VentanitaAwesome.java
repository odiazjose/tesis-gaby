import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JSpinner;
import java.awt.event.ActionListener;
import java.util.Arrays;
import java.awt.event.ActionEvent;
import javax.swing.SpinnerNumberModel;

import net.miginfocom.swing.MigLayout;
import java.awt.Font;

public class VentanitaAwesome {
	private Resultados resultWindow;
	private JFrame frmAwesomeAndCool;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					VentanitaAwesome window = new VentanitaAwesome();
					window.frmAwesomeAndCool.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public VentanitaAwesome() {
		initialize();
		resultWindow = new Resultados();
		resultWindow.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmAwesomeAndCool = new JFrame();
		frmAwesomeAndCool.getContentPane().setBackground(Color.PINK);
		frmAwesomeAndCool.setTitle("Awesome and cool on pink");
		frmAwesomeAndCool.setBounds(100, 100, 765, 470);
		frmAwesomeAndCool.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JSpinner PotenciaTransmision = new JSpinner();
		PotenciaTransmision.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		
		JSpinner Gananci = new JSpinner();
		Gananci.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		
		JSpinner VX = new JSpinner();
		VX.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		
		JSpinner Sensibil = new JSpinner();
		Sensibil.setModel(new SpinnerNumberModel(new Float(0), null, null, new Float(1)));
		
		JSpinner LongitudSala = new JSpinner();
		LongitudSala.setModel(new SpinnerNumberModel(new Float(0), new Float(0), null, new Float(1)));
		
		JSpinner AnchoSala = new JSpinner();
		AnchoSala.setModel(new SpinnerNumberModel(new Float(0), new Float(0), null, new Float(1)));
		
		JSpinner AltoSala = new JSpinner();
		AltoSala.setModel(new SpinnerNumberModel(new Float(0), new Float(0), null, new Float(1)));
		
		JSpinner FrecuenciaReloj = new JSpinner();
		
		JSpinner TBackoff = new JSpinner();
		
		JSpinner VY = new JSpinner();
		VY.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		
		JSpinner VZ = new JSpinner();
		VZ.setModel(new SpinnerNumberModel(new Double(0), new Double(0), null, new Double(1)));
		
		JSpinner BSRx = new JSpinner();
		BSRx.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(1)));
		
		JSpinner ProbabilTx = new JSpinner();
		ProbabilTx.setModel(new SpinnerNumberModel(new Float(0), new Float(0), null, new Float(1)));
		
		JSpinner estacionesMoviles = new JSpinner();
		estacionesMoviles.setModel(new SpinnerNumberModel(new Integer(0), new Integer(0), null, new Integer(0)));
		
		JSpinner TSim = new JSpinner();
		
		JSpinner PacketsTx = new JSpinner();

		JButton btnSimular = new JButton("Simular");
		btnSimular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				double[] InfoInicio= new double[16];
				InfoInicio[0]= getFloat(estacionesMoviles);
				InfoInicio[1]= getFloat(PotenciaTransmision);
				InfoInicio[2]= getFloat(Gananci);
				InfoInicio[3]= getFloat(Sensibil);
				InfoInicio[4]= getFloat(AnchoSala);
				InfoInicio[5]= getFloat(LongitudSala);
				InfoInicio[6]= getFloat(AltoSala);
				InfoInicio[7]= getFloat(FrecuenciaReloj);
				InfoInicio[8]= getFloat(TBackoff);
				InfoInicio[9]= getFloat(VX);
				InfoInicio[10]= getFloat(VY);
				InfoInicio[11]= getFloat(VZ);
				InfoInicio[12]= getFloat(ProbabilTx);
				InfoInicio[13]= getFloat(BSRx);
				InfoInicio[14]= getFloat(TSim);
				InfoInicio[15]= getFloat(PacketsTx);
				// Exito :)
				
				Simulador Sim=new Simulador();
				float[] DataSalida= new float[6];
				DataSalida=Arrays.copyOf(Sim.Simulacion(InfoInicio), 6);
				
				System.out.println("El numero de Sesiones Exitosas es de: " + DataSalida[0]);
				System.out.println("El numero de Sesiones Fallidas es de: " + DataSalida[1]);
				System.out.println("El numero de Transmisiones Exitosas es de: " + DataSalida[2]);
				System.out.println("El numero de Alineaciones Exitosas es de: " + DataSalida[4]);
				System.out.println("El numero de Alineaciones Fallidas es de: " + DataSalida[5]);

				// Agregar resultados a la ventana de resultados
				Object[] rowData = new Object[7];
				rowData[0] = null;
				rowData[1] = DataSalida[0];
				rowData[2] = DataSalida[1];
				rowData[3] = DataSalida[2];
				rowData[4] = DataSalida[4];
				rowData[5] = DataSalida[5];
				rowData[6] = getFloat(TSim);
				
				resultWindow.addResultRow(rowData);
			}
		});
		MigLayout layout = new MigLayout("gapx 10px, gapy 10px", "[][grow,fill][grow,fill][grow,fill][grow,fill][]", "[20px,grow,center][20px,grow][20px,grow][53.00px,grow][23px,grow]");
		frmAwesomeAndCool.getContentPane().setLayout(layout);
		
		JLabel lblPotenciaDeTransmisin = new JLabel("Potencia de transmisi\u00F3n");
		lblPotenciaDeTransmisin.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblPotenciaDeTransmisin, "flowy,cell 1 1,grow");
		frmAwesomeAndCool.getContentPane().add(PotenciaTransmision, "cell 1 1,grow");
		
		JLabel Ganancia = new JLabel("Ganancia de Antenas");
		Ganancia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(Ganancia, "flowy,cell 1 2,grow");
		frmAwesomeAndCool.getContentPane().add(Gananci, "cell 1 2,grow");
		
		JLabel Velx = new JLabel("Vel. Max. en Eje X");
		Velx.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(Velx, "flowy,cell 4 0,alignx left,growy");
		frmAwesomeAndCool.getContentPane().add(VX, "cell 4 0,grow");
		
		JLabel Sensibilidad = new JLabel("Sensibilidad de Recepcion");
		Sensibilidad.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(Sensibilidad, "flowy,cell 3 2,grow");
		frmAwesomeAndCool.getContentPane().add(Sensibil, "cell 3 2,grow");
		
		JLabel lblLongitudDeSala = new JLabel("Longitud de sala (m)");
		lblLongitudDeSala.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblLongitudDeSala, "flowy,cell 2 1,grow");
		frmAwesomeAndCool.getContentPane().add(LongitudSala, "cell 2 1,grow");
		
		JLabel lblAnchoDeLa = new JLabel("Ancho de la sala (m)");
		lblAnchoDeLa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblAnchoDeLa, "flowy,cell 2 2,grow");
		frmAwesomeAndCool.getContentPane().add(AnchoSala, "cell 2 2,grow");
		
		JLabel lblAltoDeLa = new JLabel("Alto de la sala (m)");
		lblAltoDeLa.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblAltoDeLa, "flowy,cell 2 0,grow");
		frmAwesomeAndCool.getContentPane().add(AltoSala, "cell 2 0,grow");
		
		JLabel lblFrecuenciaDeReloj = new JLabel("Numero de Simulaciones");
		lblFrecuenciaDeReloj.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblFrecuenciaDeReloj, "flowy,cell 3 1,grow");
		frmAwesomeAndCool.getContentPane().add(FrecuenciaReloj, "cell 3 1,grow");
		
		JLabel label_1 = new JLabel("Tiempo de backoff");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(label_1, "flowy,cell 3 0,grow");
		frmAwesomeAndCool.getContentPane().add(TBackoff, "cell 3 0,grow");
		
		JLabel VelY = new JLabel("Vel. Max. en Eje Y");
		VelY.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(VelY, "flowy,cell 4 1,alignx left,growy");
		frmAwesomeAndCool.getContentPane().add(VY, "cell 4 1,grow");
		
		JLabel VelZ = new JLabel("Vel. Max. en Eje Z");
		VelZ.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(VelZ, "flowy,cell 4 2,alignx left,growy");
		frmAwesomeAndCool.getContentPane().add(VZ, "cell 4 2,grow");
		
		JLabel CapacidadBS = new JLabel("Capacidad de Recepcion BS");
		CapacidadBS.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(CapacidadBS, "flowy,cell 1 3,grow");
		frmAwesomeAndCool.getContentPane().add(BSRx, "cell 1 3,grow");
		
		JLabel ProbTx = new JLabel("Probabilidad de Transmisi\u00F3n");
		ProbTx.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(ProbTx, "flowy,cell 2 3,grow");
		frmAwesomeAndCool.getContentPane().add(ProbabilTx, "cell 2 3,grow");
		
		JLabel lblTiempoDeSimulacion = new JLabel("Tiempo de Simulacion");
		lblTiempoDeSimulacion.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblTiempoDeSimulacion, "flowy,cell 3 3,grow");
		frmAwesomeAndCool.getContentPane().add(TSim, "cell 3 3,grow");
		
		JLabel lblMaximoDePaquetes = new JLabel("Maximo de Paquetes por Sesion");
		lblMaximoDePaquetes.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblMaximoDePaquetes, "flowy,cell 4 3,grow");
		frmAwesomeAndCool.getContentPane().add(PacketsTx, "cell 4 3,grow");
		frmAwesomeAndCool.getContentPane().add(btnSimular, "cell 2 4 2 1,alignx center,aligny center");
		
		JLabel lblEstacionesMviles = new JLabel("Estaciones m\u00F3viles");
		lblEstacionesMviles.setFont(new Font("Tahoma", Font.PLAIN, 14));
		frmAwesomeAndCool.getContentPane().add(lblEstacionesMviles, "flowy,cell 1 0,alignx left,growy");
		
		frmAwesomeAndCool.getContentPane().add(estacionesMoviles, "cell 1 0,grow");
		
		JLabel label = new JLabel(":)");
		frmAwesomeAndCool.getContentPane().add(label, "cell 4 4");
	}
	private float getFloat(JSpinner spinner) { 
		return ((Number) spinner.getValue()).floatValue();
	}
}
