import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import java.awt.GridLayout;
import java.util.Arrays;

import javax.swing.JList;
import javax.swing.JComboBox;
import net.miginfocom.swing.MigLayout;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.BevelBorder;
import javax.swing.JLabel;
import javax.swing.AbstractListModel;
import javax.swing.border.CompoundBorder;
import javax.swing.JScrollPane;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.JFreeChart;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import javax.swing.event.ListSelectionEvent;

public class Resultados extends JFrame {

	private String[] comboOptions;
	private Object[][] results;
	
	private JPanel contentPane;
	private JTable table;
	
	private JList list;

	/**
	 * Launch the application.
	 *//*
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					resultados frame = new resultados();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}*/

	/**
	 * Create the frame.
	 */
	public Resultados() {
		setTitle("Resultados");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 600, 400);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new MigLayout("", "[][486.00,grow,fill][146.00,grow,fill][][17.00]", "[][][][][][grow][]"));
		
		JLabel lblResultados = new JLabel("Resultados");
		contentPane.add(lblResultados, "cell 1 1");
		
		JScrollPane scrollPane = new JScrollPane();
		contentPane.add(scrollPane, "flowx,cell 1 3 1 3");
		
		results = new Object[0][7];
		table = new JTable();
		scrollPane.setViewportView(table);
		table.setModel(new DefaultTableModel(
			results,
			new String[] {
				"Simulaci\u00F3n", "Sesiones exitosas", "Sesiones fallidas", "Transmisiones exitosas", "Alineaciones exitosas", "Alineaciones fallidas", "Tiempo"
			}
		) {
			boolean[] columnEditables = new boolean[] {
				false, false, false, false, false, false, false
			};
			public boolean isCellEditable(int row, int column) {
				return columnEditables[column];
			}
		});
		
		JLabel lblGraficar = new JLabel("Graficar");
		contentPane.add(lblGraficar, "cell 2 4");
		
		JScrollPane scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, "flowy,cell 2 5,growy");
		
		JFreeChart chart;
		ChartFrame frame;
		XYSeriesCollection dataset = new XYSeriesCollection();
	    
    	chart = ChartFactory.createXYAreaChart("Transmisiones exitosas", "Tiempo de sesi�n m�ximo", "N�mero de transmisiones exitosas", dataset);
		frame = new ChartFrame("Transmisiones exitosas", chart);
		frame.pack();
		frame.setVisible(true);
		
		comboOptions = new String[0];
		list = new JList();
		list.addListSelectionListener(new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent e) {
				if (e.getValueIsAdjusting()) return;
				
				dataset.removeAllSeries();
				XYSeries series1 = new XYSeries("Gr�fica 1", true);
			    series1.add(0, 0);
			    dataset.addSeries(series1);
				
				int[] indices = list.getSelectedIndices();
				
				for (int index: indices) {
					Object[] row = results[index];
					
					series1.add((Float) row[6], (Float) row[3]);
				}
			}
		});	

		scrollPane_1.setViewportView(list);
		list.setModel(new AbstractListModel() {
			String[] values = comboOptions;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
	}
	
	public void addResultRow(Object[] rowData) {
		Integer newIndex = results.length;
		Object [][] newResults = new Object[newIndex + 1][6];
		
		for (int i = 0; i < results.length; i++) {
			newResults[i] = results[i];
		}
		
		rowData[0] = newIndex + 1;
		newResults[newIndex] = rowData;
		results = newResults;
		
		table.setModel(new DefaultTableModel(
				results,
				new String[] {
					"Simulaci\u00F3n", "Sesiones exitosas", "Sesiones fallidas", "Transmisiones exitosas", "Alineaciones exitosas", "Alineaciones fallidas", "Tiempo"
				}
			) {
				boolean[] columnEditables = new boolean[] {
					false, false, false, false, false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		
		String newOption = rowData[0].toString();
		addComboOption(newOption);
	}
	
	public void addComboOption(String newOption) {
		Integer newIndex = comboOptions.length;
		String [] newOptions = new String[newIndex + 1];

		for (int i = 0; i < comboOptions.length; i++) {
			newOptions[i] = comboOptions[i];
		}
		
		newOptions[newIndex] = newOption;
		comboOptions = newOptions;
		
		int[] selectedIndices = list.getSelectedIndices();
		
		list.setModel(new AbstractListModel() {
			String[] values = comboOptions;
			public int getSize() {
				return values.length;
			}
			public Object getElementAt(int index) {
				return values[index];
			}
		});
		
		list.setSelectedIndices(selectedIndices);
	}

}
