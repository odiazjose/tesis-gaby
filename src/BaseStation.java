import java.util.Arrays;
public class BaseStation {
private double[] pos = new double[3];
public float[][] AllAntennaDir;
private float Gmax;
private float Power;
private int[] PrioritySector;
private boolean[] AlineationStarted;
private double MejorSINR[];
public float[][] AllOptimumDir;
public double HPBW;
public int[] SectoresEscaneados;
public BaseStation(int NAntena, float Altura, float Ganancia){
	AllAntennaDir = new float[NAntena][2];
	PrioritySector = new int[NAntena];
	AlineationStarted= new boolean[NAntena];
	MejorSINR=new double[NAntena];
	AllOptimumDir= new float[NAntena][2];
	SectoresEscaneados=new int[NAntena];
	this.Gmax=Ganancia;
		this.pos[0]=0;
	this.pos[1]=0;
	this.pos[2]=Altura;
	this.Power=10;
	this.HPBW=Math.sqrt(31000*Math.pow(10, -this.Gmax/10));
	for(int z=0; z<NAntena; z++){
		this.PrioritySector[z]=5;
		this.MejorSINR[z]=-100;
		this.AlineationStarted[z]=false;
		this.AllAntennaDir[z][0]=0;
		this.AllAntennaDir[z][1]=0;
	}
}
public int getFreeArray(){
	int NL=0;
	for(float[] Chequeo:this.AllAntennaDir){
		if (Chequeo[0]==0){
			if(Chequeo[1]==0){
			return NL;
			}
		}
	NL=NL+1;
	}
	return -1;
}
public double[] getPos() {
	return pos;
}
public void setPos(double[] pos) {
	this.pos = Arrays.copyOf(pos, 3);
}
public float[] getAntennaDir(int Asignacion) {
	return AllAntennaDir[Asignacion];
}
public void setAntennaDir(int Asignation, float[] Apuntar) {
	this.AllAntennaDir[Asignation]= Arrays.copyOf(Apuntar, 2);
}
public float[] getOptimumDir(int Asignacion) {
	return AllOptimumDir[Asignacion];
}
public void setOptimumDir(int Asignation, float[] TheWay) {
	this.AllOptimumDir[Asignation]= Arrays.copyOf(TheWay, 2);
}
public int getPrioritySector (int Asignacion) {
	return PrioritySector[Asignacion];
}
public void setPrioritySector(int Asignation, int Sector) {
	this.PrioritySector[Asignation]= Sector;
}
public double getSINROptimo (int Asignacion) {
	return MejorSINR[Asignacion];
}
public void setSINROptimo(int Asignation, double SINROpt) {
	this.MejorSINR[Asignation]= SINROpt;
}
public boolean getAlineationStart (int Asignacion) {
	return AlineationStarted[Asignacion];
}
public void setAlineationStart(int Asignation, boolean Inicio) {
	this.AlineationStarted[Asignation]= Inicio;
}
public float getPow() {
	return Power;
}
public double GetGanreal(double[] posNodoB, int AntenaAsig){
double[] direcreal=new double [2];
double[] posrel=new double[3];
posrel[0]=posNodoB[0]-this.pos[0];
posrel[1]=posNodoB[1]-this.pos[1];
posrel[2]=posNodoB[2]-this.pos[2];
if(posrel[0]<0){
	if(posrel[1]<0){
		direcreal[0]=direcreal[0]-Math.PI;
	}
	else{
		direcreal[0]=Math.PI-direcreal[0];
	}
}
direcreal[0]=Math.atan(posrel[1]/posrel[0]);
direcreal[1]=Math.atan(posrel[2]/Math.sqrt(posrel[0]*posrel[0]+posrel[1]*posrel[1]));
double[] direcrelativa=new double[2];
direcrelativa[0]=Math.abs(direcreal[0]-Math.toRadians(this.AllAntennaDir[AntenaAsig][0]));
direcrelativa[1]=Math.abs(direcreal[1]-Math.toRadians(this.AllAntennaDir[AntenaAsig][1]));

double Ganreal = 0;
double N=Math.toDegrees(Math.acos(Math.cos(direcrelativa[0])*Math.cos(direcrelativa[1])));
double Na=0;
double HPBW2=0;
if(direcrelativa[0]<Math.toRadians(this.HPBW)){
	HPBW2=this.HPBW;
}
else if(Math.abs(direcrelativa[0])<Math.PI){
	HPBW2=1/Math.sqrt((Math.cos((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)*(Math.cos((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)+(Math.sin((Math.abs(Math.toDegrees(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW)*(Math.sin((Math.toDegrees(Math.abs(direcrelativa[0]))-this.HPBW)*90/(180-this.HPBW))/this.HPBW));
}
if (N<90){
	Na= this.HPBW;
}
else if (N<180){
	Na= 1/(Math.sqrt((Math.cos(direcrelativa[1])/HPBW2)*(Math.cos(direcrelativa[1])/HPBW2)+(Math.sin(direcrelativa[1])/this.HPBW)*(Math.sin(direcrelativa[1])/this.HPBW)));
}
double x=N/Na;
if (x<=1){
	Ganreal=Gmax-12*x*x;
}
else{
	Ganreal=Gmax-12-15*Math.log(Math.abs(x));
}
return Ganreal;
}
public double Rx(double Ganreal, double GanMT, double[] posNodoB, float powMT){
	double[] posrel=new double[3];
	posrel[0]=posNodoB[0]-this.pos[0];
	posrel[1]=posNodoB[1]-this.pos[1];
	posrel[2]=posNodoB[2]-this.pos[2];
	double distancia = Math.sqrt(posrel[0]*posrel[0]+posrel[1]*posrel[1]+posrel[2]*posrel[2])*0.001;
	double Powrec=(powMT-(92.44+20*Math.log10(60)+22*Math.log10(distancia))+Ganreal+GanMT);
	return Powrec;
}
}
